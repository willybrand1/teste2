@section('titulo','Home')
        <!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">
    <title> @yield('titulo') </title>

    <!-- Fontfaces CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/font-face.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/font-awesome-4.7/css/font-awesome.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/font-awesome-5/css/fontawesome-all.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/mdi-font/css/material-design-iconic-font.min.css')}}"/>

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap-4.1/bootstrap.min.css')}}"/>

    <!-- Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/animsition/animsition.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/wow/animate.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/css-hamburgers/hamburgers.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/slick/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/select2/select2.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/datatables.min.css')}}"/>

    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/theme.css')}}"/>
</head>

<body class="animsition">
<div>

@if(!Auth::guest())
    @include('menu-mobile')
    <!-- PAGE CONTAINER-->
        <div class="page-container">
        @include('menu')
        <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            @yield('conteudo')
                        </div>
                        @include('footer')
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>
@else
    <div class="container-fluid">
        <div class="row">
            @yield('conteudo')
        </div>
    </div>
@endif
</div>

<!-- Jquery JS-->
<script src="{{asset('vendor/jquery-3.2.1.min.js')}}"></script>
<!-- Bootstrap JS-->
<script src="{{asset('vendor/bootstrap-4.1/popper.min.js')}}"></script>
<script src="{{asset('vendor/bootstrap-4.1/bootstrap.min.js')}}"></script>
<!-- Vendor JS -->
<script src="{{asset('vendor/slick/slick.min.js')}}"></script>
<script src="{{asset('vendor/wow/wow.min.js')}}"></script>
<script src="{{asset('vendor/animsition/animsition.min.js')}}"></script>
<script src="{{asset('vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
<script src="{{asset('vendor/counter-up/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('vendor/counter-up/jquery.counterup.min.js')}}"></script>
<script src="{{asset('vendor/circle-progress/circle-progress.min.js')}}"></script>
<script src="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
<script src="{{asset('vendor/chartjs/Chart.bundle.min.js')}}"></script>
<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
<script src="{{asset('vendor/DataTables/datatables.min.js')}}"></script>
<script src="{{asset('js/jquery.mask.js')}}"></script>

<!-- Main JS-->
<script src="{{asset('js/main.js')}}"></script>
<style>
    table tr td{
        font-size: 12px;
    }

    #tabelaSistemas_length{
        text-align: right;
    }
</style>
<script>
$(document).ready(function(){
    styleMenu();
    // ambienteEstadoOnLoad();

    var slcImpacto = $("#slcImpacto").val();
    mostraSistemas(slcImpacto);

    //tabela de equipes
    var table = $("#tabelaSistemas").DataTable({
        dom: '<"row"<"col-sm-9"f><"col-sm-3"l>>t'+'<"row"<"col-sm-6"B><"col-sm-6"p>>'+'ri',
        // dom: 'lftBrip',
        processing: true,
        serverSide: true,
        scrollX:true,
        // scrollCollapse:true,
        // responsive: true,
        buttons: [
            { extend: 'colvis', text:'Colunas', className: 'btn-outline-primary', columns: ':not(.noVis)' },
            { extend: 'csv', className: 'btn-outline-primary' },
            { extend: 'pdf', className: 'btn-outline-primary' },
        ],
        ajax:{ 
            url: "{{ route('sistemas.getdata') }}", 
            dataType: "json",
            type: "GET",
        },
        columns: [
            { data:"sistema" },
            { data:"sigla" },
            { data:"class_desc" },
            { data:"porte" },
            { data:"visi_politica" },
            { data:"visi_social" },
            { data:"pdti_pei" },
            { data:"pactuado" },
            { data:"risco_op" },
            { data:"impacto_sis" },
            { data:"repositorio" },
            { data:"descricao" },
            { data:"tipo_proj" },
            { data:"grupo" },
            { data:"amb_teste" },
            { data:"amb_teste_url" },
            { data:"amb_teste_aplic" },
            { data:"amb_teste_db" },
            { data:"amb_homol" },
            { data:"amb_homol_url" },
            { data:"amb_homol_aplic" },
            { data:"amb_homol_db" },
            { data:"amb_prod" },
            { data:"amb_prod_url" },
            { data:"amb_prod_aplic" },
            { data:"amb_prod_db" },
            { data:"banco" },
            { data:"arquitetura" },
            { data:"coordenador" },
            { data:"substituto" },
            { data:"owner_sis" },
            { data:"analista" },
            { data:"qualidade" },
            { data:"linguagem" },
            { data:"sec_especial" },
            { data:"diretoria" },
            { data:"sigla_sec" },
            { data:"gestor" },
            { data:"status_sit" },
            { data:"inicio" },
            { data:"previsao" },
            { data:"basis_gp" },
            { data:"action" }
        ],
        oLanguage: {
            "sProcessing":   "Processando...",
            "sLengthMenu":   "Mostrar _MENU_ registros",
            "sZeroRecords":  "N&atilde;o foram encontrados resultados",
            "sInfo":         "<span style='font-size:14px;'>Mostrando de _START_ at&eacute; _END_ de _TOTAL_ registros</span>",
            "sInfoEmpty":    "Mostrando de 0 at&eacute; 0 de 0 registros",
            "sInfoFiltered": "(filtrado de _MAX_ registros no total)",
            "sInfoPostFix":  "",
            "sSearch":       "<span style='font-size:14px;'>Buscar</span>:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "<span style='font-size:14px;'>Primeiro</span>",
                "sPrevious": "<span style='font-size:14px;'>Anterior</span>",
                "sNext":     "<span style='font-size:14px;'>Seguinte</span>",
                "sLast":     "<span style='font-size:14px;'>&Uacute;ltimo</span>"
            }
        },
        'columnDefs': [
            { "targets": 1,"visible": false },
            { "targets": 2,"visible": false },
            { "targets": 3,"visible": false },
            { "targets": 4,"visible": false },
            { "targets": 5,"visible": false },
            { "targets": 6,"visible": false },
            { "targets": 7,"visible": false },
            { "targets": 8,"visible": false },
            { "targets": 9,"visible": false },
            { "targets": 10,"visible": false },
            { "targets": 11,"visible": false },
            { "targets": 12,"visible": false },
            { "targets": 13,"visible": false },
            { "targets": 14,"visible": false },
            { "targets": 15,"visible": false },
            { "targets": 16,"visible": false },
            { "targets": 17,"visible": false },
            { "targets": 18,"visible": false },
            { "targets": 19,"visible": false },
            { "targets": 20,"visible": false },
            { "targets": 21,"visible": false },
            { "targets": 22,"visible": false },
            { "targets": 23,"visible": false },
            { "targets": 24,"visible": false },
            { "targets": 25,"visible": false },
            { "targets": 26,"visible": false },
            { "targets": 27,"visible": false },
            { "targets": 28,"visible": false },
            { "targets": 29,"visible": false },
            { "targets": 30,"visible": false },
            { "targets": 31,"visible": false },
            { "targets": 32,"visible": false },
            { "targets": 33,"visible": false },
            { "targets": 42,"searchable": false,"orderable": false,"className":"text-center" },
        ]
    });
    // table.buttons().container().appendTo($('#btnCol'));
    
    var table = $("#tabelaGrupo").DataTable({
        dom: '<"row"<"col-sm-9"f><"col-sm-3"l>>t'+'<"row"<"col-sm-6"B><"col-sm-6"p>>'+'ri',
        // dom: 'lftBrip',
        processing: true,
        serverSide: true,
        scrollX:true,
        scrollCollapse:true,
        responsive: true,
        buttons: [
            { extend: 'csv', className: 'btn-outline-primary' },
            { extend: 'pdf', className: 'btn-outline-primary' },
        ],
        ajax:{ 
            url: "{{ route('grupo.getdata') }}", 
            dataType: "json",
            type: "POST",
            headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        },
        columns: [
            { data:"cod_grupo" },
            { data:"grupo" },
            { data:"action" }
        ],
        oLanguage: {
            "sProcessing":   "Processando...",
            "sLengthMenu":   "Mostrar _MENU_ registros",
            "sZeroRecords":  "N&atilde;o foram encontrados resultados",
            "sInfo":         "<span style='font-size:14px;'>Mostrando de _START_ at&eacute; _END_ de _TOTAL_ registros</span>",
            "sInfoEmpty":    "Mostrando de 0 at&eacute; 0 de 0 registros",
            "sInfoFiltered": "(filtrado de _MAX_ registros no total)",
            "sInfoPostFix":  "",
            "sSearch":       "<span style='font-size:14px;'>Buscar</span>:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "<span style='font-size:14px;'>Primeiro</span>",
                "sPrevious": "<span style='font-size:14px;'>Anterior</span>",
                "sNext":     "<span style='font-size:14px;'>Seguinte</span>",
                "sLast":     "<span style='font-size:14px;'>&Uacute;ltimo</span>"
            }
        },
        'columnDefs': [
            { "targets": 2,"searchable": false,"orderable": false,"className":"text-center" },
        ]
    });

    var table = $("#tabelaUser").DataTable({
        dom: '<"row"<"col-sm-9"f><"col-sm-3"l>>t'+'<"row"<"col-sm-6"B><"col-sm-6"p>>'+'ri',
        // dom: 'lftBrip',
        processing: true,
        serverSide: true,
        scrollX:true,
        scrollCollapse:true,
        responsive: true,
        buttons: [
            { extend: 'csv', className: 'btn-outline-primary' },
            { extend: 'pdf', className: 'btn-outline-primary' },
        ],
        ajax:{ 
            url: "{{ route('user.getdata') }}", 
            dataType: "json",
            type: "GET",
        },
        columns: [
            { data:"name" },
            { data:"email" },
            { data:"mobile" },
            { data:"tipo" },
            { data:"perfil" },
            { data:"action" }
        ],
        oLanguage: {
            "sProcessing":   "Processando...",
            "sLengthMenu":   "Mostrar _MENU_ registros",
            "sZeroRecords":  "N&atilde;o foram encontrados resultados",
            "sInfo":         "<span style='font-size:14px;'>Mostrando de _START_ at&eacute; _END_ de _TOTAL_ registros</span>",
            "sInfoEmpty":    "Mostrando de 0 at&eacute; 0 de 0 registros",
            "sInfoFiltered": "(filtrado de _MAX_ registros no total)",
            "sInfoPostFix":  "",
            "sSearch":       "<span style='font-size:14px;'>Buscar</span>:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "<span style='font-size:14px;'>Primeiro</span>",
                "sPrevious": "<span style='font-size:14px;'>Anterior</span>",
                "sNext":     "<span style='font-size:14px;'>Seguinte</span>",
                "sLast":     "<span style='font-size:14px;'>&Uacute;ltimo</span>"
            }
        },
        'columnDefs': [
            { "targets": 5,"searchable": false,"orderable": false,"className":"text-center" },
        ]
    });

    var table = $("#tabelaSecretaria").DataTable({
        dom: '<"row"<"col-sm-9"f><"col-sm-3"l>>t'+'<"row"<"col-sm-6"B><"col-sm-6"p>>'+'ri',
        // dom: 'lftBrip',
        processing: true,
        serverSide: true,
        scrollX:true,
        scrollCollapse:true,
        responsive: true,
        buttons: [
            { extend: 'csv', className: 'btn-outline-primary' },
            { extend: 'pdf', className: 'btn-outline-primary' },
        ],
        ajax:{ 
            url: "{{ route('secretaria.getdata') }}", 
            dataType: "json",
            type: "GET",
        },
        columns: [
            { data:"id_sec" },
            { data:"sec_especial" },
            { data:"sigla_sec" },
            { data:"action" }
        ],
        oLanguage: {
            "sProcessing":   "Processando...",
            "sLengthMenu":   "Mostrar _MENU_ registros",
            "sZeroRecords":  "N&atilde;o foram encontrados resultados",
            "sInfo":         "<span style='font-size:14px;'>Mostrando de _START_ at&eacute; _END_ de _TOTAL_ registros</span>",
            "sInfoEmpty":    "Mostrando de 0 at&eacute; 0 de 0 registros",
            "sInfoFiltered": "(filtrado de _MAX_ registros no total)",
            "sInfoPostFix":  "",
            "sSearch":       "<span style='font-size:14px;'>Buscar</span>:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "<span style='font-size:14px;'>Primeiro</span>",
                "sPrevious": "<span style='font-size:14px;'>Anterior</span>",
                "sNext":     "<span style='font-size:14px;'>Seguinte</span>",
                "sLast":     "<span style='font-size:14px;'>&Uacute;ltimo</span>"
            }
        },
        'columnDefs': [
            { "targets": 3,"searchable": false,"orderable": false,"className":"text-center" },
        ]
    });

    var table = $("#tabelaDiretoria").DataTable({
        dom: '<"row"<"col-sm-9"f><"col-sm-3"l>>t'+'<"row"<"col-sm-6"B><"col-sm-6"p>>'+'ri',
        // dom: 'lftBrip',
        processing: true,
        serverSide: true,
        scrollX:true,
        scrollCollapse:true,
        responsive: true,
        buttons: [
            { extend: 'csv', className: 'btn-outline-primary' },
            { extend: 'pdf', className: 'btn-outline-primary' },
        ],
        ajax:{ 
            url: "{{ route('diretoria.getdata') }}", 
            dataType: "json",
            type: "GET",
        },
        columns: [
            { data:"cod_diretoria" },
            { data:"sec_especial" },
            { data:"diretoria" },
            { data:"sigla_dir" },
            { data:"action" }
        ],
        oLanguage: {
            "sProcessing":   "Processando...",
            "sLengthMenu":   "Mostrar _MENU_ registros",
            "sZeroRecords":  "N&atilde;o foram encontrados resultados",
            "sInfo":         "<span style='font-size:14px;'>Mostrando de _START_ at&eacute; _END_ de _TOTAL_ registros</span>",
            "sInfoEmpty":    "Mostrando de 0 at&eacute; 0 de 0 registros",
            "sInfoFiltered": "(filtrado de _MAX_ registros no total)",
            "sInfoPostFix":  "",
            "sSearch":       "<span style='font-size:14px;'>Buscar</span>:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "<span style='font-size:14px;'>Primeiro</span>",
                "sPrevious": "<span style='font-size:14px;'>Anterior</span>",
                "sNext":     "<span style='font-size:14px;'>Seguinte</span>",
                "sLast":     "<span style='font-size:14px;'>&Uacute;ltimo</span>"
            }
        },
        'columnDefs': [
            { "targets": 4,"searchable": false,"orderable": false,"className":"text-center" },
        ]
    });

    $("#txtContato").mask('(99) 99999-9999');

    $('#tabelaSistemas tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
    } );

    $('#tabelaGrupo tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
    } );

    $('#tabelaUser tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
    } );

    $('#tabelaSecretaria tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
    } );

    $('#tabelaDiretoria tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
    } );
 
    $('#button').click( function () {
        alert( table.rows('.selected').data().length +' row(s) selected' );
    } );

    $('.js-example-basic-multiple').select2({
        theme: "classic"
    });

    $(".js-example-basic-single").select2({
        theme: 'bootstrap4'
    });

    $("#slcLinguagem").select2({
        theme: 'bootstrap4',
        tags: true
    });

    $("#slcBanco").select2({
        theme: 'bootstrap4',
        tags: true
    });

    $("#slcArquitetura").select2({
        theme: 'bootstrap4',
        tags: true
    });

    $("#slcTipoAmbiente1").click(function(){
        if($(this).is(":checked")){
            $("#txtServDB1").removeAttr("disabled");
            $("#txtServAplic1").removeAttr("disabled");
            $("#txtUrl1").removeAttr("disabled");
        }else{
            $("#txtServDB1").prop("disabled",true);
            $("#txtServAplic1").prop("disabled",true);
            $("#txtUrl1").prop("disabled",true);
        }
    });

    $("#slcTipoAmbiente2").click(function(){
        if($(this).is(":checked")){
            $("#txtServDB2").removeAttr("disabled");
            $("#txtServAplic2").removeAttr("disabled");
            $("#txtUrl2").removeAttr("disabled");
        }else{
            $("#txtServDB2").prop("disabled",true);
            $("#txtServAplic2").prop("disabled",true);
            $("#txtUrl2").prop("disabled",true);
        }
    });

    $("#slcTipoAmbiente3").click(function(){
        if($(this).is(":checked")){
            $("#txtServDB3").removeAttr("disabled");
            $("#txtServAplic3").removeAttr("disabled");
            $("#txtUrl3").removeAttr("disabled");
        }else{
            $("#txtServDB3").prop("disabled",true);
            $("#txtServAplic3").prop("disabled",true);
            $("#txtUrl3").prop("disabled",true);
        }
    });

    if($("#slcTipoAmbiente1").is(":checked")){
        $("#txtServDB1").removeAttr("disabled");
        $("#txtServAplic1").removeAttr("disabled");
        $("#txtUrl1").removeAttr("disabled");
    }else{
        $("#txtServDB1").prop("disabled",true);
        $("#txtServAplic1").prop("disabled",true);
        $("#txtUrl1").prop("disabled",true);
    }

    if($("#slcTipoAmbiente2").is(":checked")){
        $("#txtServDB2").removeAttr("disabled");
        $("#txtServAplic2").removeAttr("disabled");
        $("#txtUrl2").removeAttr("disabled");
    }else{
        $("#txtServDB2").prop("disabled",true);
        $("#txtServAplic2").prop("disabled",true);
        $("#txtUrl2").prop("disabled",true);
    }

    if($("#slcTipoAmbiente3").is(":checked")){
        $("#txtServDB3").removeAttr("disabled");
        $("#txtServAplic3").removeAttr("disabled");
        $("#txtUrl3").removeAttr("disabled");
    }else{
        $("#txtServDB3").prop("disabled",true);
        $("#txtServAplic3").prop("disabled",true);
        $("#txtUrl3").prop("disabled",true);
    }

    if(($("#txtServDB2").val() !== "") || ($("#txtServAplic2").val() !== "") || ($("#txtUrl2").val() !== "")){
        $("#slcTipoAmbiente2").prop("checked",true);
        $("#txtServDB2").removeAttr("disabled");
        $("#txtServAplic2").removeAttr("disabled");
        $("#txtUrl2").removeAttr("disabled");
    }

    if(($("#txtServDB3").val() !== "") || ($("#txtServAplic3").val() !== "") || ($("#txtUrl3").val() !== "")){
        $("#slcTipoAmbiente3").prop("checked",true);
        $("#txtServDB3").removeAttr("disabled");
        $("#txtServAplic3").removeAttr("disabled");
        $("#txtUrl3").removeAttr("disabled");
    }

    $("#slcSecretaria").change(function(){
        const diretoria = "{{ old('slcSecretaria') }}";
    });
    const diretoria = "{{ old('slcSecretaria') }}";
    const diretoriaInput = ($("#cod_diretoria").val() !== "") ? $("#cod_diretoria").val() : "";

    $("#slcSecEspecial").change(function(){
        if($(this).val() !== ""){
            var valor = $(this).val();
            $.ajax({
                url: "{{ route('sistemas.getdiretoria') }}",
                method: 'GET',
                dataType: 'json',
                data: {
                    valor:valor
                },
                success: function(obj){
                    $("#slcSecretaria").removeAttr("disabled");
                    $("#slcSecretaria").find('option').remove();

                    $('#slcSecretaria').append("<option value=''>&nbsp;</option>");
                    $.each(obj, function (key, val) {
                        if((diretoria == val.cod_diretoria) || (diretoriaInput == val.cod_diretoria)){
                            $('#slcSecretaria').append(new Option(val.diretoria, val.cod_diretoria,false,true));
                        }else{
                            $('#slcSecretaria').append(new Option(val.diretoria, val.cod_diretoria,false,false));
                        }
                    });
                }
            });
        }else{
            $("#slcSecretaria").prop("disabled",true);
            $("#slcSecretaria").find('option').remove();
        }
    });

    if($("#slcSecEspecial").val() !== ""){
        var valor = $("#slcSecEspecial").val();
        $.ajax({
            url: "{{ route('sistemas.getdiretoria') }}",
            method: 'GET',
            dataType: 'json',
            data: {
                valor:valor
            },
            success: function(obj){
                $("#slcSecretaria").removeAttr("disabled");
                $("#slcSecretaria").find('option').remove();
                
                $('#slcSecretaria').append("<option value=''>&nbsp;</option>");
                $.each(obj, function (key, val) {
                    if((diretoria == val.cod_diretoria) || (diretoriaInput == val.cod_diretoria)){
                        $('#slcSecretaria').append(new Option(val.diretoria, val.cod_diretoria,false,true));
                    }else{
                        $('#slcSecretaria').append(new Option(val.diretoria, val.cod_diretoria,false,false));
                    }
                });
            }
        });
    }else{
        $("#slcSecretaria").prop("disabled",true);
        $("#slcSecretaria").find('option').remove();
    }

    geraChart("{{ route('admin.getchartdata') }}","canvas","pie","Linguagens",1);
    geraChart("{{ route('admin.getchartdata') }}","canvas2","pie","Bancos",2);
    geraChart("{{ route('admin.getchartdata') }}","canvas3","pie","Arquiteturas",3);
    geraChart("{{ route('admin.getchartdata') }}","canvas4","bar","Coordenadores",4,1);
});
</script>
</body>
</html>
